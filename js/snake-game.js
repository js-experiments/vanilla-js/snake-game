document.body.addEventListener('keydown', (e) => {
  if (e.key === ' ') {
    e.preventDefault();
  }
});

const Tiles = {
  Snake: 1,
  Empty: 2,
  Food: 3,
  Wall: 4,
};

const Directions = {
  Up: 1,
  Down: 2,
  Left: 3,
  Right: 4,
}

// Including walls
// so 2x2 will generate a board with 1 empty tile
// 3x3 will generate a board with 4 empty tiles
// 4x4 will generate a board with 9 empty tiles
// nxn will generate a board with (n - 1) ** 2 tiles
// nxm will generate a board with (n - 1) * (m - 1) tiles
const snakes = (width, height) => {
  const subscribers = [];
  const snake = [];
  const boardSize = (width - 1) * (height - 1);
  let board = [];
  let intervalId;
  let direction;
  let head;
  let tail;
  let food;
  let gameOver = false;

  const tile = ({
    x,
    y,
    type
  }) => ({
    position: {
      x,
      y
    },
    type
  });

  const createBoard = () => {
    board = Array.from({
        length: height + 1
      },
      (v, i) => Array.from({
          length: width + 1
        },
        (v, j) => tile({
          x: j,
          y: i,
          type: i === 0 || i === height ||
            j === 0 || j === width ?
            Tiles.Wall : Tiles.Empty,
        }))
    );

    setInitialPositions();
  };

  const setInitialPositions = () => {
    const x = Math.floor(width / 2);
    const y = Math.floor(height / 2)
    head = {
      x,
      y
    };
    tail = {
      x,
      y
    };

    board[head.y][head.x].type = Tiles.Snake;
    snake.unshift(board[head.y][head.x]);

    food = getRandomFood();
    board[food.y][food.x].type = Tiles.Food;
  };

  const getRandomFood = () => {
    const empty = board
      .reduce((list, t) => {
        list.push(...t);
        return list;
      }, [])
      .filter(t => t.type === Tiles.Empty);

    const rand = Math.round(Math.random() * (empty.length - 1));

    const {
      x,
      y
    } = !empty[rand] ? {
      x: 0,
      y: 0
    } : empty[rand].position;

    return {
      x,
      y
    };
  }

  const updateDirection = (newDirection) => {
    if (
      gameOver ||
      direction === Directions.Up && (newDirection === Directions.Up || newDirection === Directions.Down) ||
      direction === Directions.Down && (newDirection === Directions.Up || newDirection === Directions.Down) ||
      direction === Directions.Left && (newDirection === Directions.Left || newDirection === Directions.Right) ||
      direction === Directions.Right && (newDirection === Directions.Left || newDirection === Directions.Right)
    ) {
      return;
    }

    direction = newDirection;

    updateBoard();
  };

  const updateBoard = () => {
    clearTimeout(intervalId);
    const changes = move();

    if (changes.length) {
      notify('BOARD_UPDATED', changes, head);
      const speed = Math.floor(700 * (1 - (snake.length / boardSize) / 1.333));

      if (snake.length < boardSize) {
        intervalId = setTimeout(updateBoard, speed);
      } else {
        gameOver = true;
        notify('GAME_OVER');
      }
    } else {
      gameOver = true;
      notify('GAME_OVER');
    }
  };

  const move = () => {
    const changes = [];
    // move head to the next position based on direction
    switch (direction) {
      case Directions.Up:
        head.y -= 1;
        break;
      case Directions.Down:
        head.y += 1;
        break;
      case Directions.Left:
        head.x -= 1;
        break;
      default:
        head.x += 1;
    }
    const {
      x,
      y
    } = head;
    const newHead = board[y][x];

    // if head hit the wall, game over
    // if snake hit itself, game over
    // If snake size equals boardSize, game over
    if (
      newHead.type === Tiles.Wall ||
      newHead.type === Tiles.Snake
    ) {
      return [];
    }

    // if snake hit food, don't update the tail
    // and push the food tile into changes array
    if (newHead.type === Tiles.Food) {
      food = getRandomFood();
      board[food.y][food.x].type = Tiles.Food;
      changes.push(board[food.y][food.x]);
    } else {
      tail = snake.pop();
      tail.type = Tiles.Empty;
      changes.push(tail);
    }

    newHead.type = Tiles.Snake;
    snake.unshift(newHead);
    changes.push(newHead);

    // You win!!!
    if (snake.length === boardSize) {
      changes.shift();
    }

    return changes;
  };

  const notify = (event, ...args) => subscribers
    .filter(s => s.event === event)
    .forEach(s => {
      s.callback.apply(this, [event, ...args])
    });

  return {
    subscribe(event, callback) {
      subscribers.push({
        event,
        callback,
      });

      return this;
    },
    dispatch(e, data) {
      notify(e, data);

      return this;
    },
    new() {
      createBoard();
      notify('GAME_CONFIGURED', {
        board
      });

      return this;
    },
    changeDirection(direction) {
      updateDirection(direction);

      return this;
    }
  }
}

const snakeRenderer = (board, boardElement) => {
  const subscribers = [];
  const controlKeys = ['a', 'd', 's', 'w'];
  let inputControl;
  let tiles = [];
  let direction;
  let head;

  const notify = (event, ...args) => subscribers
    .filter(s => s.event === event)
    .forEach(s => s.callback.apply(this, [event, ...args]));

  const getElementClassName = (type) => {
    switch (type) {
      case Tiles.Snake:
        return '--snake';
      case Tiles.Food:
        return '--food';
      case Tiles.Wall:
        return '--wall';
      default:
        return '--empty';
    }

    return '';
  }

  const createInputControl = () => {
    inputControl = document.body;
    inputControl.addEventListener('keyup', changeDirection);

    inputControl.addEventListener('touchend', (e) => {
      e.preventDefault();

      if (e.touches.length === 0 && e.changedTouches.length === 1) {
        const [x, y] = e
          .changedTouches[0]
          .target
          .dataset
          .position
          .split(',')
          .map(p => +p);
        const {
          x: hX,
          y: hY
        } = head;
        const hDiff = x - hX;
        const vDiff = y - hY;
        let directionVerifier

        if (!direction) {
          direction = Math.abs(hDiff) >= Math.abs(vDiff) ?
            hDiff >= 0 ? Directions.Right : Directions.Left :
            vDiff >= 0 ? Directions.Down : Directions.Up;
        } else if (direction === Directions.Left || direction === Directions.Right) {
          direction = vDiff >= 0 ? Directions.Down : Directions.Up;
        } else {
          direction = hDiff >= 0 ? Directions.Right : Directions.Left;
        }

        notify('DIRECTION_CHANGED', direction);
      }
    });

  };

  const renderBoard = () => {
    createInputControl();
    tiles = board.map(row => {
      const rowDiv = document.createElement('div');
      rowDiv.classList.add('row');
      const rowElements = row.map(tile => {
        const div = document.createElement('div');

        div.classList.add('board__tile', getElementClassName(tile.type));
        div.dataset.position = `${tile.position.x},${tile.position.y}`;
        rowDiv.appendChild(div);
        if (tile.type === Tiles.Snake) {
          head = {
            ...tile.position,
          };
        }
        return div;
      });
      boardElement.appendChild(rowDiv);
      return rowElements;
    });
  }

  const findElement = ({
    x,
    y
  }) => tiles[y][x];

  const updateBoard = (changes, {
    x,
    y
  }) => {
    head = {
      x,
      y
    };

    changes.forEach(({
      position,
      type
    }) => {
      const elm = findElement(position);
      elm.classList.remove('--food', '--snake', '--empty');
      elm.classList.add(getElementClassName(type));
    });
  }

  const changeDirection = (e) => {
    if (controlKeys.indexOf(e.key) < 0) {
      return;
    }

    e.preventDefault();

    switch (e.key) {
      case 'w':
        direction = Directions.Up;
        break;
      case 'a':
        direction = Directions.Left;
        break;
      case 's':
        direction = Directions.Down;
        break;
      default:
        direction = Directions.Right;
    }

    notify('DIRECTION_CHANGED', direction);
  }

  return {
    subscribe(event, callback) {
      subscribers.push({
        event,
        callback,
      });

      return this;
    },
    render(board) {
      renderBoard();

      return this;
    },
    move(changes, head) {
      updateBoard(changes, head);

      return this;
    }
  }
}

const boardElement = document.querySelector('.board');
let renderer;

const game = snakes(10, 10)
  .subscribe('GAME_CONFIGURED', (e, {
    board
  }) => (
    renderer = snakeRenderer(board, boardElement)
    .subscribe('DIRECTION_CHANGED', (e, direction) => game.changeDirection(direction))
    .render()
  ))
  .subscribe('BOARD_UPDATED', (e, changes, head) => renderer.move(changes, head))
  .subscribe('GAME_OVER', (e) => console.log(e))
  .new();
